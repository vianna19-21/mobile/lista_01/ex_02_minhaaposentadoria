package br.edu.vianna.ex_02_minhaaposentadoria.models;

import android.widget.Button;

import java.util.Calendar;
import java.util.Date;

public class Aposentadoria {
    private int tempoServico, idade;

    public boolean podeAposentar() {
        if ((idade >= 65) || (tempoServico >= 30) || (idade >= 60 && tempoServico >= 25 )) {
            return true;
        }

        return false;
    }

    public Aposentadoria() {
    }

    public Aposentadoria(int idade, int tempoServico) {
        this.tempoServico = tempoServico;
        this.idade = idade;
    }

    public int getTempoServico() {
        return tempoServico;
    }

    public void setTempoServico(int tempoServico) {
        this.tempoServico = tempoServico;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }
}
