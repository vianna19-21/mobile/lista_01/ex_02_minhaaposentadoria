package br.edu.vianna.ex_02_minhaaposentadoria;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

import br.edu.vianna.ex_02_minhaaposentadoria.models.Aposentadoria;

public class MainActivity extends AppCompatActivity {
    private TextInputEditText txtIdade, txtTempoServico;
    private Button btnChecar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("Minha Aposentadoria");

        bindings();

        btnChecar.setOnClickListener(callChecarAposentadoria());
    }

    private View.OnClickListener callChecarAposentadoria() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String msg;
                Aposentadoria apo = new Aposentadoria(
                        Integer.parseInt(txtIdade.getText().toString()),
                        Integer.parseInt(txtTempoServico.getText().toString())
                );

                if (apo.podeAposentar()) {
                    msg = "Parabéns! Você já pode se aposentar!";
                } else {
                    msg = "Você ainda não pode se aposentar.";
                }

                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();

            }
        };
    }

    private void bindings() {
        txtIdade = findViewById(R.id.txtIdade);
        txtTempoServico = findViewById(R.id.txtTempoServico);
        btnChecar = findViewById(R.id.btnChecar);
    }
}